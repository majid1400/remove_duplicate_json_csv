import glob
import json
import os
import pandas as pd
from utility.decorators import timer
import re


class Remove_duplicates:
	def __init__(self, filename):
		self.char_fa = ["ء", "آ", "ی", "ه", "و", "ن", "م", "ل", "گ", "ک", "ق", "ف", "غ", "ع", "ظ", "ط", "ض", "ص", "ش",
		                "س", "ژ",
		                "ز", "ر", "ذ", "د", "خ", "ح", "چ", "ج", "ث", "ت", "پ", "ب", "ا", "ى", " "]
		self.filename = filename
	
	def cleaner_for_key(self, text):
		r = ""
		f = list(
			text.replace("ي", "ی").replace("ى", "ی").replace("ة", "ه").replace("ۀ", "ه").replace("ؤ", "و").replace("إ",
			                                                                                                       "ا").replace(
				"أ", "ا").replace("ك", "ک").replace("ـ", "").replace("ئ", "ی").replace("ّ", "").replace("ً",
			                                                                                            "").replace("ٌ",
			                                                                                                        "").replace(
				"ٍ", "").replace("َ", "").replace("ُ", "").replace("ِ", ""))
		for j in f:
			if j not in self.char_fa:
				j = " "
			r = r + j
		return (re.sub(' +', ' ', r).strip())
	
	@timer
	def remove_duplicate_csv(self):
		data = pd.read_csv(self.filename).reset_index()
		data['message_summary'] = data.apply(lambda x: self.cleaner_for_key(x['Message'][:150]), axis=1)
		data.drop_duplicates(subset="message_summary", keep=False, inplace=True, ignore_index=True)
		data = data.drop(["index", "row", "message_summary"], axis=1)
		data.to_csv(self.filename, index=True, encoding='utf-8', quoting=1, mode="w", index_label='row')
		return len(data)
	
	@timer
	def merge_json(self):
		head_tail = os.path.split(self.filename)
		read_files = glob.glob("{}/*.json".format(head_tail[0]))
		
		result = []
		c = 0
		for v, f in enumerate(read_files):
			with open(f, 'rb') as file:
				result.append(json.load(file))
				c += 1
				print(c)
			os.remove(f)
		with open("{}/data.json".format(head_tail[0]), 'w', encoding='utf-8') as file:
			json.dump(result, file, ensure_ascii=False, indent=4)
		self.remove_duplicate_json()
	
	@timer
	def remove_duplicate_json(self):
		seen = set()
		result = []
		with open(self.filename, encoding='utf-8') as file:
			res = json.load(file)
			for k, d in enumerate(res):
				h = d.copy()
				h.pop('From')
				h = tuple(h.items())
				if h not in seen:
					result.append(d)
					seen.add(h)
			
			with open(self.filename, 'w', encoding='utf-8') as file:
				json.dump(result, file, ensure_ascii=False, indent=4)
